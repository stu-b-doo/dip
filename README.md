# dip: Determine the go import path for a directory

[![GoDoc](https://godoc.org/gitlab.com/stu-b-doo/dip?status.svg)](https://godoc.org/gitlab.com/stu-b-doo/dip)

[Import paths on golang.org](https://golang.org/doc/code.html#ImportPaths)


# Install

`go get gitlab.com/stu-b-doo/dip/cmd/dip`

# Usage

`dip <path>` Defaults to path of current directory. 

# Godoc script

I use this as part of a script to open godocs for the package I'm currently working with:

```
#!/usr/bin/sh
# Try import path for the provided directory
impa="$(dip $1)"
shift
# Open the address in browser
start "http://127.0.0.1:6060/pkg/"$impa
# Start godocs server, pass in remaining args, release the terminal
godoc -http=:6060 "$@" &
```
