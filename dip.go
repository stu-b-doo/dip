// Package dip returns the go package import path for a directory
package dip

import (
	"os"
	fp "path/filepath"
	"strings"
)

// DetermineImportPath from an absolute directory path
func DetermineImportPath(dir string) (importPath string, err error) {

	// import paths relative to either GOPATH or GOROOT paths TODO confirm no others
	gopath, _ := os.LookupEnv("GOPATH")
	// TODO try goroot
	// goroot, ok := os.LookupEnv("GOROOT")
	// if ok != true {
	// 	fmt.Println("ERROR: Env var $GOROOT not set")
	// }

	// try gopath/src/ first
	path, err := fp.Rel(fp.Join(gopath, "src"), dir)
	if err != nil || AboveBase(path) {
		// TODO if multiple base paths are tried, need to return that none of them satisfied dir
		return "", err
	}

	// TODO try goroot

	// import paths have forward slash
	return fp.ToSlash(path), nil

}

// AboveBase returns whether a relative file path resolves to a directory above the base path (ie contains ../ )
// this seems hacky but I can't find a path/filepath way to do it
func AboveBase(relPath string) bool {

	// upDir indicates that a relative path is above the base path
	upDir := fp.FromSlash("../")

	// if a relative path contains ../, it resolves to a point above its base path
	return strings.Contains(relPath, upDir)
}
